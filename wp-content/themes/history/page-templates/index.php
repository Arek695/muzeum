<?php
/**
 * Template Name: Ekspozycja
 *
 * Displays the About Us page template.
 *
 * @package Theme Freesia
 * @subpackage Photograph
 * @since Photograph 1.0
 */
get_header();
?>
    <style>
        .test1{
            display: none;
            position:absolute;
        }
        .test2{
            display: none;
            position:absolute;
        }
        .test3{
            display: none;
            position:absolute;
        }
        .test4{
            display: none;
            position:absolute;
        }
        .test5{
            display: none;
            position:absolute;
        }
        .test6{
            display: none;
            position:absolute;
        }
        .test7{
            display: none;
            position:absolute;
        }
        .test8{
            display: none;
            position:absolute;
        }
        .test9{
            display: none;
            position:absolute;
        }
        .test10{
            display: none;
            position:absolute;
        }
        .test11{
            display: none;
            position:absolute;
        }
        .test12{
            display: none;
            position:absolute;
        }
        .test13{
            display: none;
            position:absolute;
        }
        .test14{
            display: none;
            position:absolute;
        }
        .hotspot1, .hotspot2, .hotspot3, .hotspot4, .hotspot5, .hotspot6, .hotspot7, .hotspot8, .hotspot9, .hotspot10, .hotspot11, .hotspot12, .hotspot13, .hotspot14 {
            /*border: 5px solid blue;*/
            z-index: 9999;
        }
        .hotspot1:hover + .test1{
            display: block;
            color: black;
        }
        .hotspot1:active + .test1{
            display: block;
            color: black;
        }
        .hotspot2:hover + .test2{
            display: block;
            color: black;
        }
        .hotspot3:hover + .test3{
            display: block;
            color: black;
        }
        .hotspot4:hover + .test4{
            display: block;
            color: black;
        }
        .hotspot5:hover + .test5{
            display: block;
            color: black;
        }
        .hotspot6:hover + .test6{
            display: block;
            color: black;
        }
        .hotspot7:hover + .test7{
            display: block;
            color: black;
        }
        .hotspot8:hover + .test8{
            display: block;
            color: black;
        }
        .hotspot9:hover + .test9{
            display: block;
            color: black;
        }
        .hotspot10:hover + .test10{
            display: block;
            color: black;
        }
        .hotspot11:hover + .test11{
            display: block;
            color: black;
        }
        .hotspot12:hover + .test12{
            display: block;
            color: black;
        }
        .hotspot13:hover + .test13{
            display: block;
            color: black;
        }
        .hotspot14:hover + .test14{
            display: block;
            color: black;
        }
    </style>
<div>
    <p style="margin-top: 40px; font-size: 28px; text-align: center;"><b>MAPA BITEW I BOJÓW KAWALERII NA PRZEDPOLU WISŁY W SIERPNIU 1920 ROKU</b></p>

    <div style="position: relative;">
        <img style="position: relative; display: block;" src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/sama-mapa-scaled.jpg"/>
        <div class="hotspot1" style="position:absolute;z-index: 20; width: 5%; height: 2%; top: 31%; left: 37%"></div>
        <div class="test1" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 31% + 25px); left: calc(37% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Oto dialog testowy dla mapki oraz muzeum kawalerii.Lorem ipsumOto dialog testowy dla mapki oraz muzeum kawalerii.Lorem ipsum</p>
        </div>

        <div class="hotspot2" style="position:absolute;z-index: 20; width: 5%; height: 2%; top: 34%; left: 39%"></div>
        <div class="test2" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 34% + 25px); left: calc(39% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog drugi</p>
        </div>

        <div class="hotspot3" style="position:absolute;z-index: 20; width: 5%; height: 2%; top: 37%; left: 39%"></div>
        <div class="test3" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 36% + 25px); left: calc(39% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog trzeci. Testowy. Tutaj można pisać albo robić zdjęcia.</p>
        </div>

        <div class="hotspot4" style="position:absolute;z-index: 20; width: 5%; height: 2%; top: 40%; left: 39%"></div>
        <div class="test4" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 36% + 25px); left: calc(39% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog czwarty. Równie dobrze rozbudowany.</p>
        </div>

        <div class="hotspot5" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 44%; left: 49%"></div>
        <div class="test5" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 44% + 25px); left: calc(49% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog piaty. Tym razem znacznie mniejszy </p>
        </div>

        <div class="hotspot6" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 44%; left: 49%"></div>
        <div class="test6" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 44% + 25px); left: calc(49% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog szósty. Dużo większy, zawiera bardzo dużo treści, jest responsywny tak jak wszystkie.Lorem ipsum, ipsum lorem.</p>
        </div>

        <div class="hotspot7" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 48%; left: 49%"></div>
        <div class="test7" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 48% + 25px); left: calc(49% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog szósty. Dużo większy, zawiera bardzo dużo treści, jest responsywny tak jak wszystkie.Lorem ipsum, ipsum lorem.</p>
        </div>

        <div class="hotspot8" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 51%; left: 52%"></div>
        <div class="test8" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 51% + 25px); left: calc(52% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog ósmy. Dużo większy, Dużo większy Dużo większy Dużo większy, zawiera bardzo dużo treści, jest responsywny tak jak wszystkie.Lorem ipsum, ipsum lorem.</p>
        </div>

        <div class="hotspot9" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 54%; left: 51%"></div>
        <div class="test9" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 54% + 25px); left: calc(51% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog dziewiąty. Dużo większy,użo większy,użo większy,użo większy,użo większy, Dużo większy Dużo większy Dużo większy, zawiera bardzo dużo treści, jest responsywny tak jak wszystkie.Lorem ipsum, ipsum lorem.</p>
        </div>

        <div class="hotspot10" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 59%; left: 51%"></div>
        <div class="test10" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 59% + 25px); left: calc(51% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog dziesiąty. Większy Dużo większy Dużo większy, zawiera bardzo dużo treści, jest responsywny tak jak wszystkie.Lorem ipsum, ipsum lorem.</p>
        </div>

        <div class="hotspot11" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 62%; left: 50%"></div>
        <div class="test11" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 62% + 25px); left: calc(50% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog jedenasty. Większy Dużo większy Dużo większy, zawiera bardzo dużo treści, jest responsywny tak jak wszystkie.Lorem ipsum, ipsum lorem.</p>
        </div>

        <div class="hotspot12" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 63%; left: 55%"></div>
        <div class="test12" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 63% + 25px); left: calc(55% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog dwunasty. Większy Dużo większy Dużo większy, zawiera bardzo dużo treści, jest responsywny tak jak wszystkie.Lorem ipsum, ipsum lorem.</p>
        </div>

        <div class="hotspot13" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 65%; left: 48%"></div>
        <div class="test13" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 65% + 25px); left: calc(48% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog TRZYNASTY. Większy Dużo większy Dużo większy, zawiera bardzo dużo treści, jest responsywny tak jak wszystkie.Lorem ipsum, ipsum lorem.</p>
        </div>

        <div class="hotspot14" style="position:absolute;z-index: 20; width: 4%; height: 2%; top: 66%; left: 52%"></div>
        <div class="test14" style="background-color: white; max-width: 500px ;padding: 16px 8px; border: 1px solid black; border-radius: 20px;top: calc( 66% + 25px); left: calc(52% + 10px);">
            <img src="http://muzeum-kawalerii.org/wp-content/uploads/2020/08/Muzeum_HD-1-e1597221170294.gif"/>
            <p>Dialog TRZYNASTY. Większy Dużo większy Dużo większy, zawiera bardzo dużo treści, jest responsywny tak jak wszystkie.Lorem ipsum, ipsum lorem.</p>
        </div>

    </div>

</div>
<?php get_footer();
